# Intelligence artificielle

# Master 1 Informatique

# Projet – Awélé

Indications générales

```
Date de rendu : 21 mars 2019 avant minuit
Travail en groupe de deux étudiants
Les fichiers seront déposés sur ARCHE dans une archivenom.du.bot.zip
```

## Le jeu de l’Awélé

L’Awélé est un jeu traditionnel africain. Le jeu connaît de nombreuses appellations et de nombreuses variantes. C’est
 un jeu de stratégie combinatoire abstrait qui voit s’affronter deux joueurs jouant à tour de rôle, sans information
 cachée et sans hasard. Ce projet considère la variante la plus utilisée du jeu.
 Un plateau de jeu comportant douze trous (deux lignes de six trous, une pour chaque joueur) est placé entre
 les joueurs. Dans chaque trous, on place quatre graines. L’objectif du jeu est de récolter plus de graines que son
 adversaire.
 À tour de rôle, chaque joueur va prendre toutes les graines contenues dans un trou (non vide) de son côté du plateau
 et les égrener une par une dans les trous suivants de sa ligne,puis de la ligne de son adversaire en suivant le sens
 de rotation (généralement anti-horaire).
 Si la dernière graine tombe dans un trou du côté de l’adversaire et que ce trou contient maintenant deux ou trois
 graines, le joueur récupère ces graines qui s’ajoutent à sonscore. De plus, si le trou précédent est également du
 côté de l’adversaire et que celui-ci comporte également deux ou trois graines, le joueur les récupère également. On
 continue à regarder le trou précédent jusqu’à arriver au camps du joueur actif ou jusqu’à ce qu’il y ait un nombre
 différent de deux ou trois graines.
 Si l’on égraine 12 graines ou plus, le trou d’où proviennent ces graines est « sauté » durant l’égrenage. Certains
 coup ne sont pas valides, il ne faut pas « affamer » son adversaire : si l’adversaire n’a plus de graine de son côté, il
 est obligatoire de jouer un coup qui place au moins une grainede son côté. De plus, si un coup permet de récupérer
 toutes les graines du côté de l’adversaire, le coup est valide, mais aucune graine n’est récupérée.
 La partie s’arrête quand un joueur a capturé 25 graines ou plus, qu’il reste moins de six graines au total sur la
 plateau ou qu’un joueur ne peut plus jouer de coup valide (auquel cas, il récupère toutes les graines encore de son
 côté).

## Le projet

On cherche à construire un moteur d’intelligence artificielle permettant de jouer à l’Awélé (unbot), en utilisant un
 ou plusieurs algorithmes.
 L’objectif est de construire un modèle de prise de décision permettant de choisir le plus efficacement possible le
 meilleur coup à jouer en fonction d’une situation donnée.
 La solution proposée utilisera un algorithme d’intelligence artificielle, en évaluant la situation de jeu et en utilisant
 des connaissances extraites à partir d’une base de données de coups joués. Les techniques d’intelligence artificielle
 employées ne sont pas limitées à celles étudiées en cours, ilest même vivement recommandé de faire une courte
 recherche bibliographique pour trouver des algorithmes potentiellement plus efficaces. La solution mise en œuvre
 pourra utiliser diverses méthodes d’analyse de données, derecherche opérationnelle, d’optimisation, etc. Les prises
 de décision ne devront en revanche pas être directement écrites dans le code.

## Les données

On dispose d’un ensemble de données correspondant à 303 situations de jeu observées au cours de plusieurs parties
 entre un joueur expérimenté et un joueur novice. Le joueur expérimenté a remporté toutes les parties.

Chaque observation est décrite par 14 variables. Les 12 premières correspondent à la situation du plateau de jeu (nombre de graines dans les trous, de gauche à droite pour le joueur actif et de droite à gauche pour l’adversaire).
La 13e variable est la décision prise par le joueur actif. La dernière variable indique si le joueur a gagné ou perdu
la partie.

```
A6 A5 A4 A3 A2 A
J1 J2 J3 J4 J5 J
```

## Le code existant

Le projet devra être développé en langage Java en se basant sur les classes fournies. Tout le code développé se
 trouvera dans ununiquepackagenomméawele.bot.nomdubotqui devra être créé. Cepackagedevra contenir
 une seule classe étendantBot.java. L’uniqueconstructeur de la classe ne devra comporteraucunparamètre
 et exécuter les méthodesaddAuthoretsetBotNameafin d’identifier le groupe d’étudiant pour l’évaluation du
 projet. Il est autorisé d’utiliser des bibliothèques Java existantes sur les algorithmes d’intelligence artificielleafin de
 pouvoir appliquer des méthodes d’apprentissage complexes. D’autres classes peuvent bien entendu être créées dans
 lepackage.
 Les étudiants pourront s’inspirer des packages existants et des classes qui étendent la classeBot.
 Les classes fournies contiennent un moteur de jeu d’Awélé permettant de voir s’affronter deuxbots. La classeBoard
 contient les informations sur l’état actuel du plateau de jeu. La classeMaincharge l’ensemble desbots, puis les fait
 s’affronter fait s’affronter deux par deux.
 La classeBotcontient deux méthodes abstraites. La méthodelearnest exécutée une fois au chargement dubot,
 afin de réaliser l’apprentissage et la préparation dubot. La méthodegetDecisiondoit retourner un tableau de six
 doublecorrespondant à l’efficacité supposée de chacun des six coupspossibles. Le coup joué par lebotsera le coup
 valide dont la valeur est la plus forte. Par exemple, si la méthode retourne le tableau{ 0 .95; 0.23; 0.67; 0.42; 0.35; 0.12}
 le bot jouera les graines du premier trou (valeur la plus élevée) sauf si ce coup est invalide auquel cas il jouera les
 graines du troisième trou, etc.

## Le travail demandé

Il est demandé de réaliser, en langage de programmation Java,un unique«bot» (joueur artificiel) permettant
 de jouer à l’Awélé. Il conviendra de donner un nom aubot. Le code devra être rendu dans une archive.zip.Seul
 le dossier contenant lepackageawele.bot.nomdubotdevra être envoyé. Si le projet nécessite l’utilisation d’une ou
 plusieurs bibliothèques, les fichiers.jardevront être rajouté dans l’archive.
 Un rapport de dix pages maximum est également demandé. Ce rapport devra être convenablement structuré (page
 de garde, introduction, conclusion, etc.). Il développerala recherche bibliographique réalisée et décrira le principe
 de la méthode d’apprentissage mise en œuvre. Le nom dubot sera rappelé dans le titre du rapport. Le rapport
 devra être rajouté à l’archive envoyée.

## La notation

La notation sera décomposée en quatre parties :
 — l’ingéniosité et la pertinence la solution mise en œuvre (9points) ;
 — la qualité de la programmation en langage Java (3 points) ;
 — la qualité de la rédaction du rapport (3 points) ;
 — l’efficacité face aux solutions proposées par les autres groupes d’étudiants (5 points).
 Lesbotsdes différents groupes d’étudiants seront comparés entre eux sous la forme d’un championnat : chaquebot
 affrontera chaque autrebot, lors de deux parties (chaquebotcommençant une partie), la somme des graines sur les
 deux parties déterminant le vainqueur. Une victoire sera récompensée par trois points, une égalité par un point.
 Chaque affrontement est alors répété 100 fois. Ces points serviront à réaliser un classement qui déterminera la note
 entre 1 (dernier du classement) et 5 (premier du classement). Une note de 0 sera attribuée aux groupes disqualifiés
 (botsnon conformes, envoie de plusieursbots, mauvaise organisation des fichiers, projet rendu en retard, groupes
 de trois étudiants ou plus).

## Remarques

Les questions concernant le projet sont à poser par e-mail à l’adresse :alexandre.blansche@univ-lorraine.fr.
 Malgré toute l’attention qui a été apportée à la programmation du moteur de jeu et l’utilisation de tests unitaires
 pour détecter les erreurs, le programme peut contenir des bugs. Ne pas hésiter à le signaler rapidement si tel est le
 cas.